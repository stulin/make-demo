#include <algorithms/fibbonacci.hpp>
#include <iostream>

int main() {
    auto seq = fibbonacci(7);
    for (auto elem : seq) {
        std::cout << elem << " ";
    }
    std::cout << std::endl;
}

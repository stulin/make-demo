#include "fibbonacci.hpp"

std::vector<size_t> fibbonacci(size_t number) {
    std::vector<size_t> arr;
    if (number == 0) {
        return arr;
    }
    arr.push_back(0);
    if (number == 1) {
        return arr;
    }
    arr.push_back(1);
    for (size_t i = 1; i < number; i++) {
        arr.push_back(arr[i] + arr[i-1]);
    }
    return arr;
}


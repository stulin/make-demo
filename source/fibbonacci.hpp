#ifndef FIBBONACCI
#define FIBBONACCI

#include <cstddef>
#include <vector>

std::vector<size_t> fibbonacci(size_t number);

#endif

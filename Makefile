CXX=g++
VERSION=1.0.1
MAJOR_VERSION=1
SHLIB=build/libalgorithms.so.$(VERSION)
STLIB=build/libalgorithms.a
SONAME=libalgorithms.so.$(MAJOR_VERSION)
CFLAGS=-Wall -c -O2
SOURCES=$(shell find source -name "*.cpp")
HEADERS=$(shell find source -name "*.hpp")
OBJECTS:=$(subst .cpp,.o,$(SOURCES))
OBJECTS:=$(subst source,build,$(OBJECTS))

DESTDIR=
PREFIX=/usr/local
LIBDIR=$(DESTDIR)/$(PREFIX)/lib/
INCDIR=$(DESTDIR)/$(PREFIX)/include/algorithms/

.PHONY: all clean samples install

build/%.o: source/%.cpp
	mkdir -p build/
	$(CXX) $< -o $@ $(CFLAGS)

all: $(SHLIB) $(STLIB)

$(SHLIB): $(OBJECTS)
	mkdir -p build/
	$(CXX) $^ -o $@ -shared -Wl,-soname,$(SONAME)

$(STLIB): $(OBJECTS)
	mkdir -p build/
	ar -r $@ $(OBJECTS)

install: all
	install $(SHLIB) $(STLIB) $(LIBDIR)
	mkdir -p $(INCDIR)
	install $(HEADERS) $(INCDIR)

clean:
	rm -rf build/ algorithms/ samples/fib

samples: $(SHLIB)
	ln -s -f source algorithms
	$(CXX) samples/fib.cpp -I. -lalgorithms -Lbuild/ -o samples/fib
	LD_LIBRARY_PATH=build/ ./samples/fib

